(setq gc-cons-threshold 402653184
	gc-cons-percentage 1.0
	backup-directory-alist `(("." . "~/.local/share/emacs/backups"))
	inhibit-startup-screen t
	initial-scratch-message nil
	file-name-handler-alist nil)

(require 'package)
(setq load-prefer-newer t	     ;; Always load the newest file between `.el' and `.elc'
	package--init-file-ensured t   ;; We do initialize our packages, yes
	package-archives nil	       ;; But we do not use `package.el' for installation, so disable it
	package-enable-at-startup nil) ;; Don't enable installed packages on boot
(package-initialize)

(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 0)

(set-frame-font "Terminus-9")

;; Show matching parens
(setq show-paren-delay 0)
(show-paren-mode 1)

(setq evil-want-keybinding nil)
;(setq evil-motion-state-modes nil)
(evil-mode)
(setq evil-vsplit-window-right t)
(setq evil-split-window-below t)
(evil-collection-init)
(evil-set-initial-state 'help-mode 'emacs)
(evil-set-initial-state 'info-mode 'emacs)

(vertico-mode 1)
;(ivy-rich-mode 1)
;(counsel-mode 1)
;(setq ivy-use-virtual-buffers t)
;(setq ivy-count-format "(%d/%d) ")

;; Enable vertico-multiform
(vertico-multiform-mode)

;; Configure the display per command.
;; Use a buffer with indices for imenu
;; and a flat (Ido-like) menu for M-x.
(setq vertico-multiform-commands
      '((consult-imenu buffer indexed)
        (execute-extended-command unobtrusive)))

;; Configure the display per completion category.
;; Use the grid display for files and a buffer
;; for the consult-grep commands.
(setq vertico-multiform-categories
      '((file grid)
        (consult-grep buffer)))


(setq which-key-separator " ")
(setq which-key-prefix-prefix "+")
(which-key-mode 1)

(setq tramp-default-method "ssh")
;(define-key global-map (kbd "C-c s") 'counsel-tramp)

(desktop-save-mode 1)
(savehist-mode 1)
(add-to-list 'savehist-additional-variables 'kill-ring)

;; Custom keybinding
(general-define-key
:states '(normal visual insert emacs)
:prefix "SPC"
:non-normal-prefix "M-SPC"
:keymaps 'override
;"/" '(counsel-rg :which-key "ripgrep")
"TAB" '(switch-to-prev-buffer :which-key "previous buffer")
"SPC" '(counsel-M-x :which-key "M-x")
"ff"  '(counsel-find-file :which-key "find files")
"fs"  '(save-buffer :which-key "save file")
;; buffers
"bb" '(consult-buffer :which-key "buffers list")
"bd" '(kill-buffer :which-key "delete buffer")
"bp" '(previous-buffer :which-key "previous buffer")
"bn" '(next-buffer :which-key "next buffer")
;; eval
"eb" '(eval-buffer :which-key "eval buffer")
"er" '(eval-region :which-key "eval region")
;; open
"ot" '(link-hint-open-link-at-point :which-key "open this")
"ol" '(link-hint-open-link :which-key "open link")
"om" '(link-hint-open-multiple-links :which-key "open multiple links")
"oa" '(link-hint-open-all-links :which-key "open all links")
;; layout
"ll" '(persp-load-state-from-file :which-key "load state")
"lS" '(persp-save-state-to-file :which-key "save state")
"la" '(persp-add-buffer :which-key "add buffer")
"lA" '(persp-set-buffer :which-key "set buffer")
"lc" '(persp-kill :which-key "kill")
"lk" '(persp-remove-buffer :which-key "remove buffer")
"ln" '(persp-next :which-key "next")
"lp" '(persp-prev :which-key "previous")
"lr" '(persp-rename :which-key "rename")
"ls" '(persp-switch which-key "switch")
;; tabs
"tn" '(tab-next :which-key "next tab")
"tp" '(tab-previous :which-key "previous tab")
"tt" '(tab-new :which-key "new tab")
"tf" '(find-file-other-tab :which-key "find file other tab")
"td" '(dired-other-tab :which-key "dired other tab")
"tb" '(switch-to-buffer-other-tab :which-key "buffer other tab")
"tk" '(tab-close :which-key "tab kill")
"ts" '(tab-bar-select-tab-by-name :which-key "tab select")
"tr" '(tab-rename :which-key "tab rename")
;; window
"wl" '(windmove-right :which-key "move right")
"wh" '(windmove-left :which-key "move left")
"wk" '(windmove-up :which-key "move up")
"wj" '(windmove-down :which-key "move bottom")
"w TAB"	 '(other-window :which-key "back and forth")
"w/" '(split-window-right :which-key "split right")
"w-" '(split-window-below :which-key "split bottom")
"wd" '(delete-window :which-key "delete window")
;; quitting
"qq" '(save-buffers-kill-terminal :which-key "save and quit")
;; anime
"ad" '(anime-double-del :which-key "delete watched")
"as" '(anime-sort-new :which-key "sort new anime")
;; char
"cd" '(describe-char :which-key "describe character")
"ci" '(insert-char :which-key "insert character")
;; clipboard (to match some neovim binds)
"p" '(evil-paste-after COUNT &optional REGISTER YANK-HANDLE :which-key "system clipboard paste")
"y" '(evil-yank BEG END &optional TYPE REGISTER YANK-HANDLE :which-key "system clipboard copy")
;; others
"at" '(ansi-term :which-key "open terminal")
"dh" '(delete-help :which-key "delete help window")
)

(fset 'anime-double-del		       
   "2Gdd:w\C-m w\C-i2Gdd:w\C-m w\C-i") 
(fset 'anime-sort-new		       
   " w\C-i:e\C-mG{jddgg}P:w\C-m w\C-i")
(fset 'anime-shift-top
   "dd2GP:w\C-m w\C-idd2GP:w\C-m w\C-i")


(defun delete-help () (interactive) (delete-windows-on "*Help*" t))
(defun anon/dired-xdg-open ()
  "Open the marked files using xdg-open."
  (interactive)
  (let ((file-list (dired-get-marked-files)))
    (mapc
     (lambda (file-path)
       (let ((process-connection-type nil))
         (start-process "" nil "xdg-open" file-path)))
     file-list)))

(require 'recentf)
(setq recentf-auto-cleanup 'never) ;; disable before we start recentf!
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;(persp-mode 0)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes '(modus-vivendi))
 '(custom-safe-themes
   '("cca1d386d4a3f645c2f8c49266e3eb9ee14cf69939141e3deb9dfd50ccaada79" "3199be8536de4a8300eaf9ce6d864a35aa802088c0925e944e2b74a574c68fd0" "183dfa34e360f5bc2ee4a6b3f4236e6664f4cfce40de1d43c984e0e8fc5b51ae" "0c2d7f410f835d59a0293f2a55744e9d3be13aab8753705c6ad4a9a968fb3b28" "a0415d8fc6aeec455376f0cbcc1bee5f8c408295d1c2b9a1336db6947b89dd98" "e1ad4299390cb3fc0cbf5a705442eaf08510aa947c90c8bc83b1d7308befb475" "076ee9f2c64746aac7994b697eb7dbde23ac22988d41ef31b714fc6478fee224" "d9864caf2fc6cd9bc5bed941c526a4690bf6c27f0d8c1ca28ff7e05806b57204" default))
 '(safe-local-variable-values '((eval progn (pp-buffer) (indent-buffer)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
