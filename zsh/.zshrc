# fix tramp
if [[ "$TERM" == "dumb" ]]
then
  unsetopt zle
  unsetopt prompt_cr
  unsetopt prompt_subst
  if whence -w precmd >/dev/null; then
      unfunction precmd
  fi
  if whence -w preexec >/dev/null; then
      unfunction preexec
  fi
  PS1='$ '
fi

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]} r:|[._-]=** r:|=**'
zstyle :compinstall filename '/home/brad/.zshrc'

autoload -Uz compinit
compinit

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
export KEYTIMEOUT=1
setopt appendhistory
setopt interactivecomments
# vi mode keys
bindkey -v
# bash style reverse history search
bindkey "^R" history-incremental-pattern-search-backward
# fix backspace in vi insert mode
bindkey -M viins '^?' backward-delete-char
bindkey -M viins '^H' backward-delete-char
# rebind HOME and END to do the decent thing
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
# enable ctrl-arrow for word jumping
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
# navigate up/down history
bindkey '^P' up-history
bindkey '^N' down-history
# cut/paste line
bindkey -M viins '^U' kill-whole-line
bindkey -M viins '^Y' yank
# emacs-style beginning/end of line
bindkey -M viins '^A' beginning-of-line
bindkey -M viins '^E' end-of-line
# emacs-style push-line
bindkey '^Q' push-line-or-edit

# fix some key bindings
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "^W" backward-delete-word
bindkey "^[d" kill-word
# for rxvt
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line
# for non RH/Debian xterm, can't hurt for RH/DEbian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
# for freebsd console
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
# completion in the middle of a line
bindkey '^i' expand-or-complete-prefix

# display shell mode
function zle-line-init zle-keymap-select {
    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
    RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}"
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# guix system specific
alias cfg='nvim ~/config.scm'
alias cpf='clear && pfetch'
alias gman='guix package -m ~/manifest.scm'
alias gplg='guix package --list-generations'
alias gpsg='guix package --switch-generations'
alias pcl='sudo -E guix gc'
alias pqs2='guix package --list-installed | grep -i'
alias recfg='sudo -E guix system reconfigure ~/config.scm --fallback'
alias s='guix search'
alias search='guix search'
alias sysupgr='guix pull && sudo -E guix system reconfigure ~/config.scm --fallback'
alias sysupgrslow='guix pull -c 2 && sudo -E guix system -c 2 reconfigure ~/config.scm'
alias u='sysupgr ; gman'
alias upgr='guix pull && guix package -m ~/manifest.scm --fallback'
alias upgrslow='guix pull -c 2 && guix package -c 2 -m ~/manifest.scm'
alias vman='nvim ~/manifest.scm'

# tmux
alias pp='tmux attach'
alias ppm='tmux attach -t main'
alias ppe='tmux attach -t editor'
alias pps='tmux attach -t scratch'
alias ta='tmux attach'
alias trc='nvim ~/.config/tmux/tmux.conf'

# edit
alias brc='nvim ~/.bashrc'
alias exr='nvim ~/.Xresources'
alias em='emacs -nw'
alias van='nvim /data0/anime/anime.m3u'
alias vanl='nvim /storage/anime/bradanime'
alias vanr='nvim -O scp://pfizuko//storage/anime/bradanime scp://pfizuko//storage/anime/bradanime.m3u'
alias vbanl='nvim /big/anime/bradanime'
alias vbanr='nvim -O scp://pfmasaki//big/anime/bradanime scp://pfmasaki//big/anime/bradanime.m3u'
alias vs='nvim -S'
alias nvs='nvim -S'
alias nvim-session='nvim -S'
alias vim='nvim'
alias zrc='nvim ~/.zshrc'

#config
alias c-aerc='nvim ~/.config/aerc/aerc.conf'
alias c-aerc-accounts='nvim ~/.config/aerc/accounts.conf'
alias c-alacritty='nvim ~/.config/alacritty/alacritty.yml'
alias c-bradmenu='nvim ~/bin/brad-menu'
alias c-clip='nvim ~/documents/clip'
alias c-emacs='nvim ~/.config/emacs/init.el'
alias c-fontconfig='nvim ~/.config/fontconfig/fonts.conf'
alias c-foot='nvim ~/.config/foot/foot.ini'
alias c-gtk3='nvim ~/.config/gtk-3.0/settings.ini'
alias c-guix-config='nvim ~/config.scm'
alias c-guix-manifest='nvim ~/manifest.scm'
alias c-imv='nvim ~/.config/imv/config'
alias c-kitty='nvim ~/.config/kitty/kitty.conf'
alias c-mako='nvim ~/.config/mako/config'
alias c-mimeapps='nvim ~/.config/mimeapps.list'
alias c-mpd='nvim ~/.config/mpd/mpd.conf'
alias c-mpv='nvim ~/.config/mpv/mpv.conf'
alias c-mpv-input='nvim ~/.config/mpv/input.conf'
alias c-mvi='nvim ~/.config/mvi/mpv.conf'
alias c-mvi-input='nvim ~/.config/mvi/input.conf'
alias c-ncmpcpp='nvim ~/.config/ncmpcpp/config'
alias c-ncmpcpp-bindings='nvim ~/.config/ncmpcpp/bindings'
alias c-neofetch='nvim ~/.config/neofetch/config.conf'
alias c-newsboat='nvim ~/.config/newsboat/config'
alias c-newsboat-urls='nvim ~/.config/newsboat/urls'
alias c-nvim='nvim ~/.config/nvim/init.vim'
alias c-opm='nvim ~/documents/openingsmoe.txt'
alias c-profile='nvim ~/.profile'
alias c-qutebrowser='nvim ~/.config/qutebrowser/config.py'
alias c-qutebrowser-style='nvim ~/.config/qutebrowser/stylesheet.css'
alias c-ranger='nvim ~/.config/ranger/rc.conf'
alias c-ranger-commands='nvim ~/.config/ranger/commands.py'
alias c-ranger-rifle='nvim ~/.config/ranger/rifle.conf'
alias c-sakura='nvim ~/.config/sakura/config'
alias c-ssh='nvim ~/.ssh/config'
alias c-sway='nvim ~/.config/sway/config'
alias c-termite='nvim ~/.config/termite/config'
alias c-tmux='nvim ~/.config/tmux/.tmux.conf'
alias c-transmission='nvim ~/.config/transmission/settings.json'
alias c-waybar='nvim ~/.config/waybar/config'
alias c-waybar-style='nvim ~/.config/waybar/style.css'
alias c-wofi='nvim ~/.config/wofi/config'
alias c-wofi-style='nvim ~/.config/wofi/style.css'
alias c-xresources='nvim ~/.Xresources'
alias c-zsh='nvim ~/.zshrc'

# mpv
alias cmpv='wl-paste | xargs mpv'
alias cmpvl='wl-paste | xargs mpv --pause=no --cache-secs=10 --demuxer-readahead-secs=10 --untimed'
alias cmpvnv='wl-paste | xargs mpv --no-video'
alias mpa='mpv /data0/anime/anime.m3u'
alias mpab='mpv /data0/anime/anime.m3u && bkup'
alias mpar='mpv /storage/anime/bradanime.m3u'
alias mparb='mpv /storage/anime/bradanime.m3u && bkup'
alias mpbar='mpv /big/anime/bradanime.m3u'
alias mpbarb='mpv /big/anime/bradanime.m3u && bkup'
alias mpv5.1='mpv --af=lavfi=loudnorm'
alias mpv5.12='mpv --ad-lavc-downmix=no --audio-normalize-downmix=yes --audio-channels=stereo --af=dynaudnorm'
alias mpvl='mpv --pause=no --cache-secs=10 --demuxer-readahead-secs=10 --untimed'
alias mpvlyt='mpv --stream-buffer-size=16MiB -script-opts=throttled-rate=100k --ytdl-format="22/18/17/bv+ba" --cache=no --volume=60'
alias mpvl2='mpv --pause=no --stream-buffer-size=16MiB -script-opts=throttled-rate=100k --cache=no --volume=60'
#alias mpvl2='mpv --pause=no --cache-secs=0 --demuxer-readahead-secs=0 --untimed'
alias mpvh='mpv --vf="hflip"'
alias mpvv='mpv --vf="vflip"'
alias mpvm='mpv --no-video --pause=no'
alias mpvm60='mpv --no-video --pause=no --volume=60'
alias mpvm70='mpv --no-video --pause=no --volume=70'
alias mpvm80='mpv --no-video --pause=no --volume=80'
alias mpvmfv='mpv --no-video --pause=no --volume=100'
alias mpvmloop='mpv --no-video --pause=no --loop-file=inf'
alias mpvm60loop='mpv --no-video --pause=no --volume=60 --loop-file=inf'
alias mpvm70loop='mpv --no-video --pause=no --volume=70 --loop-file=inf'
alias mpvm80loop='mpv --no-video --pause=no --volume=80 --loop-file=inf'
alias mpvmfvloop='mpv --no-video --pause=no --volume=100 --loop-file=inf'
alias mpvr='mpv --ytdl-raw-options=playlist-reverse='
alias mpyt='mpv --force-window=immediate --playlist=$HOME/youtube.m3u'

# screenshots
alias mscrot='mv -t ~/scrot ~/*scrot.png ; mv -t ~/scrot ~/Downloads/*Shot*.png'
alias fullscrot="grim ~/scrot/\$(date +'%Y-%m-%d-%H%M%S_grim_desktop.png')"

# misc
alias bkup='cd ~/ && rsync -avuL ~/config.scm ~/documents/anpan-guix-config.scm && rsync -avuL ~/manifest.scm ~/documents/anpan-guix-manifest.scm && rsync -avuL ~/.config/emacs/init.el ~/documents/anpan-guix-init.el && rsync -avuL ~/.config/nvim/init.vim ~/documents/anpan-guix-init.vim && rsync -avuL ~/.config/sway/config ~/documents/anpan-guix-sway-config && rsync -avuL ~/.config/qutebrowser/config.py ~/documents/anpan-guix-qutebrowser-config.py && rsync -avuL ~/.config/qutebrowser/quickmarks ~/documents/anpan-guix-qutebrowser-quickmarks && rsync -avuL ~/.config/mimeapps.list ~/documents/anpan-guix-mimeapps.list && rsync -avuL ~/Sync/pass.kdbx ~/documents/.i/ && rsync -avuL ~/documents/clip ~/Sync/ && rsync -vPc /data0/anime/anime pfmasaki:/big/anime/bradanime && rsync -zzavuP video scrot mpvscrot mpvwebm documents pictures books bin youtube.m3u pfmasaki: && rsync -zzavuP /data0/camera pfmasaki: && cd -'
alias bkcf='bkup && clear && pfetch'
alias bm='BEMENU_BACKEND=curses bemenu'
alias bmr='BEMENU_BACKEND=curses bemenu-run'
alias can1='cat /storage/anime/anime1.m3u'
alias can1h='cat /storage/anime/anime1.m3u | head'
alias can1l='less -i /storage/anime/anime1.m3u'
alias canb='cat /data0/anime/anime.m3u'
alias canbh='cat /data0/anime/anime.m3u | head'
alias canbl='less -i /data0/anime/anime.m3u'
alias canbr='cat /storage/anime/bradanime.m3u'
alias canbrh='cat /storage/anime/bradanime.m3u | head'
alias canbrl='less -i /storage/anime/bradanime.m3u'
alias cdd='cd ~/Downloads/'
alias cdD='cd ~/documents/'
alias cdpma="cd ~/.local/var/pmbootstrap/cache_git/pmaports"
alias dc='~/bin/datecheck'
alias eclip='wl-paste | vipe | wl-copy'
alias ed0='export DISPLAY=:0'
alias epaste='nvim $(wl-paste)'
alias ewd0='export WAYLAND_DISPLAY=wayland-0'
alias fan='find /data0/anime -iname "*.mkv" -print | fzf -m >> /data0/anime/anime.m3u'
alias fanr='find /data0/anime -iname "*.mkv" -mtime -3 -print | fzf -m >> /data0/anime/anime.m3u'
alias fanr7='find /data0/anime -iname "*.mkv" -mtime -7 -print | fzf -m >> /data0/anime/anime.m3u'
alias fanr90='find /data0/anime -iname "*.mkv" -mtime -90 -print | fzf -m >> /data0/anime/anime.m3u'
alias ftv='find /data0/tv -iname "*.mkv" -print | fzf -m >> /data0/anime/anime.m3u'
alias ftvr='find /data0/tv -iname "*.mkv" -mtime -3 -print | fzf -m >> /data0/anime/anime.m3u'
alias ftvr7='find /data0/tv -iname "*.mkv" -mtime -7 -print | fzf -m >> /data0/anime/anime.m3u'
alias fmvr='find /data0/movies -iname "*.mkv" -mtime -3 -print | fzf -m >> /data0/anime/anime.m3u'
alias gi='grep -i'
alias gitjob='git log -n100000 --format="%ae" | cut -d@ -f2 | sort -u | less'
alias grabtunesM='cd ~/music && sftp pfmasaki:/storage/music ; cd -'
alias grep='grep --color=auto'
alias hg='history | grep -i'
alias ll='ls -hal'
alias lower="sed -e 's/[A-Z]/\L&/g'"
alias lowerc="wl-paste | lower | tr -d '\n' | wl-copy"
alias ls='ls -p --color'
alias lsr='ls -1ct | tac'
alias lsR='ls -halct | tac'
alias lss='ls -hal | grep -i'
alias lxr='xrdb ~/.Xresources'
alias ms='sshfs pfmasaki:/storage /storage2 -o reconnect,allow_other ; sshfs pfmasaki:/big /big -o reconnect,allow_other ; sudo cryptsetup open --type luks2 /dev/disk/by-uuid/cb7b1caa-0448-4a35-9d40-7ab1d82a18f1 bullet && sudo mount /dev/mapper/bullet /data0 ; ls /storage /storage2 /big /data0'
alias mvi='mpv --config-dir=$HOME/.config/mvi'
alias nf='neofetch --ascii_colors 7 --colors 7 --color_blocks off'
alias nl2s="tr '\n' ' '"
alias opm='fzy < ~/documents/openingsmoe.txt | xargs mpv'
alias opmnv='fzy < ~/documents/openingsmoe.txt | xargs mpv --no-video'
alias opmfv='fzy < ~/documents/openingsmoe.txt | xargs mpv --volume=100'
alias opmnvfv='fzy < ~/documents/openingsmoe.txt | xargs mpv --no-video --volume=100'
alias paste-sprunge="curl -F 'sprunge=<-' http://sprunge.us"
alias paste-winny="curl -F p=\<- https://p.winny.tech/"
alias pfi='ssh pfizuko'
alias pfm='ssh pfmasaki'
alias pft='ssh pftatsumaki'
alias pfgt='ssh pfgtatsumaki'
alias pm='ssh masaki'
alias ran="ranger --cmd='set sort=mtime' /big/anime /data0/anime"
alias rangerm="ranger --cmd='set sort=mtime'"
alias rtv="ranger --cmd='set sort=mtime' /big/tv /data0/tv"
alias rmv="ranger --cmd='set sort=mtime' /big/movies /data0/movies"
alias ranger-anime="ranger --cmd='set sort=mtime' /big/anime /data0/anime"
alias ranger-tv="ranger --cmd='set sort=mtime' /big/tv /data0/tv"
alias ranger-movies="ranger --cmd='set sort=mtime' /big/movies /data0/movies"
alias src='source ~/.zshrc'
alias ssf='export SWAYSOCK=$(ls /run/user/1000 | grep sway)'
alias sway-dbus='exec dbus-launch-session -- sway'
alias swaysock-fix='export SWAYSOCK=$(ls /run/user/1000 | grep sway)'
alias sxiv='nsxiv'
alias tanb='$HOME/bin/antail.py < /data0/anime/anime.m3u'
alias tanbr='$HOME/bin/antail.py < /storage/anime/bradanime.m3u'
alias tanbl='$HOME/bin/antail.py < /data0/anime/anime.m3u'
alias timestamp-unix="date + '%s' | wl-copy"
alias iup="find ~/ | fzy | xargs -d '\n' cat | up"
alias iupc="find ~/ | fzy | xargs -d '\n' cat | upc"
alias up='curl -F file=@- https://0x0.st'
alias up2='curl -nsT - chunk.io'
alias up2c='curl -nsT - chunk.io | wl-copy'
alias pup="wl-paste | curl -F file=@- https://0x0.st"
alias Pup="wl-paste -p | curl -F file=@- https://0x0.st"
alias pupc="wl-paste | curl -F file=@- https://0x0.st | nl2s | wl-copy"
alias Pupc="wl-paste -p | curl -F file=@- https://0x0.st | nl2s | wl-copy"
alias lanl='less /storage/anime/bradanime'
alias lbanl='less /big/anime/bradanime'
alias wlp='wl-paste | less'
alias wlpp='wl-paste -p | less'
alias ytdlc= "cd ~/video && wl-paste | xargs -d '\n' yt-dlp && cd -"
alias ytdlhc= "yt-dlp \$(wl-paste)"
alias ytdlmc= 'cd ~/video/music && wl-paste | xargs yt-dlp && cd'

autoload -Uz promptinit
promptinit
prompt suse

# edit command in editor
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

# By default, ^S freezes terminal output and ^Q resumes it. Disable that so
# that those keys can be used for other things.
unsetopt flowcontrol
# Run fzy in the current working directory, appending the selected path, if
# any, to the current command, followed by a space.
function insert-fzy-path-in-command-line() {
    local selected_path
    # Print a newline or we'll clobber the old prompt.
    echo
    # Find the path; abort if the user doesn't select anything.
    selected_path=$(find * -type f | fzy) || return
    # Append the selection to the current command buffer.
    eval 'LBUFFER="$LBUFFER$selected_path "'
    # Redraw the prompt since fzy has drawn several new lines of text.
    zle reset-prompt
}

# run countdown for specified amount of time, then do a backup
function cdbk { countdown "$1" && bkup && clear && pfetch }

# search guix packages, print name and version lines (adjacent)
function pqs { s "$1" | grep -A1 "name" }

# Create the zle widget
zle -N insert-fzy-path-in-command-line
# Bind the key to the newly created widget
bindkey "^S" "insert-fzy-path-in-command-line"

#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# share history among terminals
setopt share_history

# append to the history instead of overwriting
setopt append_history

# append to history incrementally instead of when the shell exits
setopt inc_append_history




[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
