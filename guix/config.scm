;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu)
	(gnu services dbus)
	(gnu services pm)
	(gnu services linux)
	(gnu services virtualization)
	(gnu system setuid)
	(guix packages))
(use-service-modules audio base desktop networking ssh sound xorg)
(use-package-modules shells wm android gnome)

(operating-system
	(locale "en_US.utf8")
	(timezone "America/Chicago")
	(keyboard-layout (keyboard-layout "us"))
	(host-name "anpan")
	(users (cons* (user-account
		(name "brad")
		(comment "Bradley")
		(group "users")
		(shell (file-append zsh "/bin/zsh"))
		(home-directory "/home/brad")
		(supplementary-groups
			'("wheel" "netdev" "audio" "video" "libvirt" "kvm" "adbusers"
			 "plugdev" "tor" "tty" "input" "dialout" "lp")))
		%base-user-accounts))

	(groups
		(cons* (user-group
			(name "adbusers")
			(system? #t))
			(user-group
			(name "plugdev")
			(system? #t))
		%base-groups))

	(packages
		(append
			(map specification->package '("adwaita-icon-theme"
							"hicolor-icon-theme"
							"gnome-themes-extra"
							"gvfs"
							"fwupd"
							"intel-vaapi-driver"
							"neovim"
							"sway"
							"swaylock"
							"tmux"
							"v4l2loopback-linux-module"
							"vim-full"))
			%base-packages))

	(services
		(cons*
			(service openssh-service-type (openssh-configuration
							(password-authentication? #t)
							(use-pam? #f)))
		(service tor-service-type)
		(service network-manager-service-type)
		(service wpa-supplicant-service-type)
		(service ntp-service-type)
		(service tlp-service-type (tlp-configuration (cpu-scaling-governor-on-ac (list "performance"))))
		(service thermald-service-type)
		(service upower-service-type)
		(service earlyoom-service-type)
		(service alsa-service-type)
		(service polkit-service-type)
		(service dbus-root-service-type)
		(service udisks-service-type)
		;(service bluetooth-service-type)
		(service libvirt-service-type
			(libvirt-configuration
				(unix-sock-group "libvirt")
				(tls-port "16555")))
		(service virtlog-service-type
			(virtlog-configuration
			(max-clients 1000)))
		(udev-rules-service 'controller-nintendo-gamecube-adapter
			(udev-rule "51-gcadapter.rules"
				(string-append
					"SUBSYSTEM==\"usb\", ENV{DEVTYPE}==\"usb_device\", "
					"ATTRS{idVendor}==\"057e\", ATTRS{idProduct}==\"0337\", "
					"MODE=\"0666\"")))
;			(service udev-service-type
;				(udev-configuration
;					(rules (cons android-udev-rules
;						(udev-configuration-rules config)))))
			(service elogind-service-type (elogind-configuration (handle-lid-switch 'ignore)))
		(service screen-locker-service-type
			 (screen-locker-configuration
			   (name "swaylock")
			   (program (file-append swaylock "/bin/swaylock"))
			   (using-pam? #t)
			   (using-setuid? #f)))
			(simple-service 'fuse-etc etc-service-type
				(list `("fuse.conf" ,(plain-file "fuse.conf" "user_allow_other\n"))))
			(simple-service 'ratbagd dbus-root-service-type (list libratbag))
	%base-services))

	(bootloader
		(bootloader-configuration
			(bootloader grub-efi-bootloader)
			(targets (list "/boot/efi"))
			(keyboard-layout keyboard-layout)))
	(swap-devices (list (swap-space
					(target "/swapfile"))))
	(mapped-devices
		(list
			(mapped-device
				(source
				(uuid "6098315e-f945-4450-8438-29cf49eea790"))
				(targets (list "cryptroot"))
				(type luks-device-mapping))))
			;(mapped-device
			;	(type luks-device-mapping)
			;	(source
			;	(uuid "b458c6b9-6781-474c-8623-ad869c02f194"))
			;	(targets (list "sidecar")))))
			;(mapped-device
			;	(type luks-device-mapping)
			;	(source
			;	(uuid "cb7b1caa-0448-4a35-9d40-7ab1d82a18f1"))
			;	(targets (list "bullet")))))

	(file-systems
		(cons* (file-system
				(mount-point "/")
				(device "/dev/mapper/cryptroot")
				(type "btrfs")
				(flags '(no-atime))
				(dependencies mapped-devices))
			(file-system
				(mount-point "/boot/efi")
				(device (uuid "F1AB-B8B3" 'fat32))
				(type "vfat"))
			;(file-system
			;	(mount-point "/data0")
			;	(dependencies mapped-devices)
			;	(device "/dev/mapper/sidecar")
			;	(type "btrfs"))
			;(file-system
			;	(mount-point "/data0")
			;	(dependencies mapped-devices)
			;	(device "/dev/mapper/bullet")
			;	(type "btrfs")
			;	(flags '(no-atime)))
	%base-file-systems))
	)
