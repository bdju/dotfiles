# Honor system-wide environment variables
source /etc/profile

# set PATH so it includes user's private bin
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

[ ! -s ~/.config/mpd/pid ] && mpd

if [[ ! -S /run/user/$UID/shepherd/socket ]]; then
    shepherd
fi

#if [ -z $DISPLAY ] && [ "$(tty)" == "/dev/tty1" ]; then
#  exec sway
#fi

export EDITOR=nvim
export VISUAL=nvim
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
export NNN_PLUG='r:renamer;f:finder;o:fzopen;p:mocplay;d:diffs;t:nmount;v:imgview'
export XDG_CURRENT_DESKTOP=Unity
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export MOZ_ENABLE_WAYLAND=1
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_CTRL_T_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_ALT_C_COMMAND='fd --type d --hidden --follow --exclude .git'
export GTK_THEME=Adwaita:dark
export QT_QPA_PLATFORMTHEME=lxqt
#export QT_QPA_PLATFORMTHEME=qt5ct
export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
export QT_LOGGING_TO_CONSOLE=1
export QT_MESSAGE_PATTERN='%{time h:mm:ss.zzz}|%{category}|%{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}|%{message}'
export TMUX_PLUGIN_MANAGER_PATH=$HOME/.local/share/tmux/plugins
export PF_COLOR=0
export BEMENU_OPTS="--no-overlap --monitor -1 --ignorecase --fn 'Terminus 9'"
export PAGER="less -R"
export MANPAGER="less -R --squeeze-blank-lines --LONG-PROMPT --use-color -Dd+G -Du+B -DSkY -DPkb"
export WEATHER_CLI_API="45247cd464d8b24f107576f7af914bba"
export LC_ALL="en_US.UTF-8"

# DISABLED
#export QT_QPA_PLATFORM=wayland
#export LC_TIME="en_SE.utf8"
#export GREP_COLOR="mt=1;32"
