set number
set wildchar=<Tab> wildmenu wildmode=full
set splitbelow splitright
set ignorecase
set smartcase
set showmatch
set undofile
set history=10000
set list listchars=nbsp:¬,tab:»·,trail:·,extends:>
let mapleader="\<SPACE>"

" jump to last position in file on open
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g`\"" | endif
endif

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif

" remove whitespace from line end
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" basics
map <leader>fs :up<CR>
map <leader>qq :q<CR>
map <leader>QQ :q!<CR>
map <leader>qa :qa<CR>
map <leader>qs :x<CR>

" move between windows with leader key
map <leader>wh :wincmd  h<CR>
map <leader>wj :wincmd  j<CR>
map <leader>wk :wincmd  k<CR>
map <leader>wl :wincmd  l<CR>
map <leader>wH :wincmd  H<CR>
map <leader>wJ :wincmd  J<CR>
map <leader>wK :wincmd  K<CR>
map <leader>wL :wincmd  L<CR>
map <leader>wd :close    <CR>
map <leader>wn :sp <CR>

" move between buffers
map <leader>bp :bprev  <CR>
map <leader>bn :bnext  <CR>
map <leader>bl :buffers  <CR>
map <leader>bd :bp<bar>sp<bar>bn<bar>bd<CR>

" move between tabs
map <leader>tp :tabprev  <CR>
map <leader>tn :tabnext  <CR>
map <leader>tt <C-w>w  <CR>

" anime management
" anime delete
map <leader>ad 2Gdd:w<CR><C-w><C-w>2Gdd:w<CR><C-w><C-w>

" anime double-delete
map <leader>aD 2G2dd:w<CR><C-w><C-w>2G2dd:w<CR><C-w><C-w>

" anime yank block + paste near bottom
map <leader>ay ggjV}yG{p$<C-v>}b<C-a>2G0

" anime yank START block + paste near bottom
map <leader>Ay ggjV}yG{pmz$<C-v>}:s/\ START//<C-m>'z$<C-v>}b<C-a>2G0

" anime yank block from current position + paste near bottom
map <leader>aY V}yG{pmz$b<C-v>}be<C-a>2G0

" anime yank START block from current position + paste near bottom
map <leader>AY V}yG{pmz$<C-v>}:s/\ START//<C-m>'z$<C-v>}b<C-a>2G0


" anime sort
function! AS1()
  wincmd w
  edit
  call timer_start(50, {-> AS2()})
endfunction

function! AS2()
  normal G{w0dGgg}P
  write
  wincmd w
endfunction

map <leader>as :call AS1()<CR>

" anime sort append block
function! AS3()
  wincmd w
  edit
  call timer_start(50, {-> AS4()})
endfunction

function! AS4()
  normal G{w0dGgg
  call search('#')
  exec "normal O\<ESC>P"
  write
  wincmd w
endfunction

map <leader>aS :call AS3()<CR>
"map <leader>aS <C-w>w:e<CR>G(dGgg/#<CR>P)kO<ESC>x:w<CR><C-w>w

" anime sort append to last block
function! AS5()
  wincmd w
  edit
  call timer_start(50, {-> AS6()})
endfunction

function! AS6()
  normal G{ee0dGgg
  call search('#')
  exec "normal (P"
  write
  wincmd w
endfunction

map <leader>aeS :call AS5()<CR>

" broken with scp files, closes second window (?!)
"map <leader>e :e<CR><C-w><C-w>:e<CR><C-w><C-w>

" tv delete
map <leader>td dd:w<CR><C-w><C-w>/^#TV$<CR>jdd:w<CR><C-w><C-w>
map <leader>tD <C-w><C-w>/^#TV$<CR>jdd:w<CR><C-w><C-w>

" tv sort
function! TS1()
  wincmd w
  edit
  call timer_start(50, {-> TS2()})
endfunction

function! TS2()
  normal G{jdd
  call search("^#TV$")
  normal }Pzz
  write
  wincmd w
endfunction

map <leader>ts :call TS1()<CR>

" tv sort block
function! TS3()
  wincmd w
  edit
  call timer_start(50, {-> TS4()})
endfunction

function! TS4()
  normal G(dG
  call search("^#TV$")
  normal }Pzz
  write
  wincmd w
endfunction

map <leader>tS :call TS3()<CR>

" movie delete
map <leader>md 2Gdd:w<CR><C-w><C-w>/^#Movies$<CR>jdd:w<CR><C-w><C-w>
map <leader>mD <C-w><C-w>/^#Movies$<CR>jdd:w<CR><C-w><C-w>

" movie sort
function! MS1()
  wincmd w
  edit
  call timer_start(50, {-> MS2()})
endfunction

function! MS2()
  normal G{jdd
  call search("^#Movies$")
  normal }Pzz
  write
  wincmd w
endfunction

map <leader>ms :call MS1()<CR>

" left-delete
map <leader>ld 2Gdd:w<CR>

" left-double-delete
map <leader>lD 2G2dd:w<CR>

" right-delete
map <leader>rd <C-w><C-w>2Gdd:w<CR><C-w><C-w>

" right-double-delete
map <leader>rD <C-w><C-w>2G2dd:w<CR><C-w><C-w>

" right-next
map <leader>rn <C-w><C-w>:bnext  <CR><C-w><C-w>

" right-previous
map <leader>rp <C-w><C-w>:bprev  <CR><C-w><C-w>

" right-top
map <leader>rt <C-w><C-w>gg2G<C-w><C-w>

" right-bottom
map <leader>rb <C-w><C-w>/#<C-m>kzb<C-w><C-w>

" left-bottom
map <leader>lb /---<C-m>jzb

" gajim quote fix
map <leader>qf 0dwxWWxwwx0j

" copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
nnoremap  <leader>yy  "+yy

" paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" search
nnoremap <leader>ss /^.*
nnoremap <leader>sq /^.*"

" find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>bb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" open url under cursor
map <leader>ot gx

" describe-char
map <leader>cd ga

call plug#begin('~/.config/nvim/autoload')
Plug 'junegunn/fzf', { 'dir': '~/.local/bin/fzf/.fzf', 'do': './install --all' }
Plug 'nikolvs/vim-sunbather'
Plug 'ishan9299/modus-theme-vim', {'branch': 'stable'}
Plug 'tjdevries/colorbuddy.nvim'
Plug 'tpope/vim-characterize'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'tpope/vim-tbone'

" nvim 0.5+ requirement
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
"Plug 'Olical/conjure'
"Plug 'kevinhwang91/nvim-hlslens'
Plug 'nvim-neorg/neorg'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
" broken for some reason
"Plug 'ctrlpvim/ctrlp.vim'
Plug 'rebelot/heirline.nvim'
"Plug 'stevearc/oil.nvim'
call plug#end()

colorscheme modus-vivendi
hi Visual cterm=reverse ctermfg=NONE ctermbg=NONE
"lua require("oil").setup()
"lua require'colorizer'.setup()
